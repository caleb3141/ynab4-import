// fuck the police
if (!Array.prototype.find) {
  Object.defineProperty(Array.prototype, 'find', {
    value: function(predicate) {
     'use strict';
     if (this == null) {
       throw new TypeError('Array.prototype.find called on null or undefined');
     }
     if (typeof predicate !== 'function') {
       throw new TypeError('predicate must be a function');
     }
     var list = Object(this);
     var length = list.length >>> 0;
     var thisArg = arguments[1];
     var value;

     for (var i = 0; i < length; i++) {
       value = list[i];
       if (predicate.call(thisArg, value, i, list)) {
         return value;
       }
     }
     return undefined;
    }
  });
}


import angular from 'angular';
import uuid from 'uuid';

let convertYfullToFinancier = () => {
  return (budgetName, yfull) => {
    const budget = new Budget(budgetName);

    const ret = [
      budget.record,
      new budget.BudgetOpened()
    ];

    const accounts = yfull.accounts
    .filter(t => !t.isTombstone)
    .map(entity => new budget.Account(entity));

    const payees = yfull.payees
    .filter(t => !t.isTombstone)
    .filter(entity => !entity.targetAccountId) // remove transfer payees
    .map(entity => new budget.Payee(entity));

    const masterCategories = yfull.masterCategories
    .filter(t => !t.isTombstone)
    .map(entity => new budget.MasterCategory(entity));

    const categories = [].concat.apply( // flatten ([[]], [[], []], ...)
      [],
      masterCategories.map(
        m => m.categories
        .filter(t => !t.isTombstone)
        .map(
          entity => new budget.Category(entity, m)
        )
      )
    ).filter(c => c);

    const transactions = yfull.transactions
    .filter(t => !t.isTombstone)
    .map(entity => new budget.Transaction(entity, accounts, categories, payees));

    const monthCategories = [].concat.apply(
      [],
      yfull.monthlyBudgets.map(
        month => {
          return month.monthlySubCategoryBudgets
          .filter(t => !t.isTombstone)
          .map(entity => new budget.MonthCategory(entity, month, categories));
        }
      )
    );

    // Flatten splits & normal transactions into a list so we can computer references
    // to eachother

    const allTransactions = transactions.slice(0).concat(
      [].concat.apply( // flatten ([[]], [[], []], ...)
        [],
        transactions.map(m => m.data.splits)
      )
    );

    allTransactions.forEach(t => {
      if (t.ynabData.transferTransactionId) {
        t.data.transfer = allTransactions.find(tr => tr.ynabData.entityId === t.ynabData.transferTransactionId).id
      }
    });

    return ret
    .concat(accounts)
    .concat(payees)
    .concat(masterCategories)
    .concat(categories)
    .concat(transactions)
    .concat(monthCategories);
  }
};

class Budget {
  constructor(budgetName) {
    this.id = uuid.v4();

    this.BudgetOpened = budgetOpened(this.id);
    this.Account = account(this.id);
    this.MasterCategory = masterCategory(this.id);
    this.Category = category(this.id);
    this.MonthCategory = monthCategory(this.id);
    this.Payee = payee(this.id);
    this.Transaction = transaction(this.id);

    this.record = {
      _id: `budget_${this.id}`,
      name: budgetName,
      currency: 'USD',
      hints: {
        outflow: true
      },
      created: new Date().toISOString(),
      checkNumber: false
    };
  }

  toJSON() {
    return this.data;
  }
}


function budgetOpened(budgetId) {
  return class BudgetOpened {
    constructor() {
      this.data = {
        _id: `budget-opened_${budgetId}`,
        opened: new Date().toISOString()
      }
    }

    toJSON() {
      return this.data;
    }
  }
}

function account(budgetId) {
  return class Account {
    constructor(ynabData) {
      this.id = uuid.v4();

      this.data = {
        _id: `b_${budgetId}_account_${this.id}`,
        onBudget: ynabData.onBudget,
        name: ynabData.accountName,
        sort: ynabData.sortableIndex,
        closed: ynabData.hidden,
        note: ynabData.note,
        type: convertAccountType(ynabData.accountType)
      };

      this.ynabData = ynabData;
    }

    toJSON() {
      return this.data;
    }
  }
}

function transaction(budgetId) {
  const SplitTransaction = splitTransaction(budgetId);

  return class Transaction {
    constructor(ynabData, accounts, categories, payees) {
      this.id = uuid.v4();

      let category, payee;

      if (ynabData.categoryId === 'Category/__ImmediateIncome__') {
        category = 'income';
      } else if (ynabData.categoryId === 'Category/__DeferredIncome__') {
        category = 'incomeNextMonth';
      } else if (ynabData.categoryId === 'Category/__Split__') {
        category = 'split';
      } else if (ynabData.categoryId) {
        category = categories.find(c => c.ynabData.entityId === ynabData.categoryId).id;
      }

      if (ynabData.payeeId && ynabData.payeeId.indexOf('Payee/Transfer:') !== 0) {
        payee = payees.find(p => p.ynabData.entityId === ynabData.payeeId).id;
      }

      this.data = {
        _id: `b_${budgetId}_transaction_${this.id}`,
        account: accounts.find(a => a.ynabData.entityId === ynabData.accountId).id,
        date: ynabData.date,
        checkNumber: ynabData.checkNumber,
        flag: convertFlags(ynabData.flag),
        memo: ynabData.memo,
        value: Math.round(ynabData.amount * 100),
        cleared: ynabData.cleared === 'Cleared' || ynabData.cleared === 'Reconciled',
        reconciled: ynabData.cleared === 'Reconciled',
        category,
        payee,
        splits: ynabData.subTransactions ? ynabData.subTransactions.map(t => {
          return new SplitTransaction(t, accounts, categories, payees);
        }) : []
      };

      this.ynabData = ynabData;
    }

    toJSON() {
      return this.data;
    }
  }
}

function splitTransaction(budgetId) {
  return class SplitTransaction {
    constructor(ynabData, accounts, categories, payees) {
      this.id = uuid.v4();

      // Splits don't have payee literals in YNAB 4

      let category;

      if (ynabData.categoryId === 'Category/__ImmediateIncome__') {
        category = 'income';
      } else if (ynabData.categoryId === 'Category/__DeferredIncome__') {
        category = 'incomeNextMonth';
      } else if (ynabData.categoryId) {
        category = categories.find(c => c.ynabData.entityId === ynabData.categoryId).id;
      }

      this.data = {
        id: this.id,
        memo: ynabData.memo,
        value: Math.round(ynabData.amount * 100),
        category,
        name: ynabData.name
      };

      this.ynabData = ynabData;
    }

    toJSON() {
      return this.data;
    }
  }
}

function monthCategory(budgetId) {
  return class MonthCategory {
    constructor(ynabData, month, categories) {
      const categoryId = categories.find(c => c.ynabData.entityId === ynabData.categoryId).id;

      let overspending = null;

      if (ynabData.overspendingHandling === 'Confined') {
        overspending = true;
      } else if (ynabData.overspendingHandling === 'AffectsBuffer') {
        overspending = false;
      }

      this.data = {
        _id: `b_${budgetId}_m_category_${month.month}_${categoryId}`,
        budget: Math.round(ynabData.budgeted * 100),
        note: ynabData.note,
        overspending
      }

      this.ynabData = ynabData;
    }

    toJSON() {
      return this.data;
    }
  }
}

function payee(budgetId) {
  return class Payee {
    constructor(ynabData) {
      this.id = uuid.v4();

      this.data = {
        _id: `b_${budgetId}_payee_${this.id}`,
        name: ynabData.name.trim()
      };

      this.ynabData = ynabData;
    }

    toJSON() {
      return this.data;
    }
  }
}

function masterCategory(budgetId) {
  return class MonthCategory {
    constructor(ynabData) {
      this.id = uuid.v4();

      this.categories = ynabData.subCategories || [];

      this.data = {
        _id: `b_${budgetId}_master-category_${this.id}`,
        name: ynabData.name,
        sort: ynabData.sortableIndex,
        note: ynabData.note
      };

      this.ynabData = ynabData;
    }

    toJSON() {
      return this.data;
    }
  }
}

function category(budgetId) {
  return class Category {
    constructor(ynabData, masterCat) {
      this.id = uuid.v4();

      this.data = {
        _id: `b_${budgetId}_category_${this.id}`,
        masterCategory: masterCat.id,
        name: ynabData.name,
        sort: ynabData.sortableIndex,
        note: ynabData.note
      };

      this.ynabData = ynabData;
    }

    toJSON() {
      return this.data;
    }
  }
}

function convertFlags(ynabColor) {
  if (ynabColor === 'Red') {
    return '#ff0000';
  } else if (ynabColor === 'Orange') {
    return '#faa710';
  } else if (ynabColor === 'Yellow') {
    return '#e5e500';
  } else if (ynabColor === 'Green') {
    return '#76b852';
  } else if (ynabColor === 'Blue') {
    return '#5276b8';
  } else if (ynabColor === 'Purple') {
    return '#b852a9';
  }

  return null;
}

function convertAccountType(accountType) {
  if (accountType === 'Checking') {
    return 'DEBIT';
  } else if (accountType === 'Mortgage') {
    return 'MORTGAGE';
  } else if (accountType === 'Savings') {
    return 'SAVINGS';
  } else if (accountType === 'CreditCard') {
    return 'CREDIT';
  } else if (accountType === 'Cash') {
    return 'CASH';
  } else if (accountType === 'LineofCredit') {
    return 'OTHERCREDIT';
  } else if (accountType === 'Paypal') {
    return 'DEBIT'; // hmmm
  } else if (accountType === 'MerchantAccount') {
    return 'MERCHANT';
  } else if (accountType === 'InvestmentAccount') {
    return 'INVESTMENT';
  } else if (accountType === 'OtherLiability') {
    return 'LOAN';
  } else if (accountType === 'OtherAsset') {
    return 'ASSET';
  }

  return null;
}






const MODULE_NAME = 'convertYfullToFinancier';

angular.module(MODULE_NAME, [])
  .factory('convertYfullToFinancier', convertYfullToFinancier);

export default MODULE_NAME;